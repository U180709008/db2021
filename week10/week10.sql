update customers set country  = replace(Country,'\n','');

select count(CustomerID),Country 
from customers
group by Country
order by count(CustomerID) desc ;

create view USA_customers as
	select CustomerName,ContactName
    from customers
    where Country = "USA";

select* from company.usa_customers;

create or replace view products_above_avg as 
select ProductName,Price
from products
where Price > (select avg(Price) from products);

select* from company.products_above_avg;