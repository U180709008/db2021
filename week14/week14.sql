use company;


create temporary table TopSuppliers(
	SupplierID int primary key,
    SupplierName varchar(45),
    ProductCount int
);

insert into TopSuppliers (SupplierID, SupplierName, ProductCount)
select suppliers.SupplierID, SupplierName, count(ProductID) as ProductCount
from suppliers join products on suppliers.SupplierID = products.SupplierID
group by SupplierName
order by ProductCount desc;

select * from TopSuppliers where ProductCount > 3;

call check_table_exists('TopSuppliers');
drop temporary table TopSuppliers;

update customers
set CustomerName="Tayyip ÖZER", City="Ankara", PostalCode="48000", Country="Turkey"
where CustomerID =1;

delete from customers where CustomerID=1;

truncate table Customers;
delete from Customers;
drop table Customers;